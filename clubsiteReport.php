<?php
//Generates excel file from results
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=excel.xls");

include ("ClubsiteConnectionObject.php");

$db = new Connection();
$connection = $db->dBConnect();

?>

<!--Must Open VPN Connect to 3.208.117.78 to run report-->

<table style="width:100%">
    <tr>
        <th>WP_Table_Num</th>
        <th>Org ID</th>
        <th>Org Name</th>
        <th>Club Site URL</th>
        <th>Facebook URL</th>
        <th>Twitter URL</th>
        <th>Instagram URL</th>
    </tr>

<?php

function countNumberOfUniqueClubTables($connection){
    $sqlQueryTableCount = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'wpprod'
                            and table_name like '%options'";
    $tableCountResults = $connection->query($sqlQueryTableCount);
    return $tableCount = $tableCountResults->fetch_assoc();
}

$tableCount = countNumberOfUniqueClubTables($connection);

//Table Counter Starts at 2 because the WP_Table without a number isn't counted in the results
$tableCounter = 2;
$wpTableNumber = 1;
while($tableCounter <= $tableCount) {

    $sqlQuery = "SELECT clubNameAndUrl.blog as clubName, clubNameAndUrl.url as url, facebook.meta_value as Facebook, twitter.meta_value as Twitter, insta.meta_value as Instagram
            FROM wpprod.wp_" . (string)$wpTableNumber . "_posts
            left join 
                (select post_id, meta_key, meta_value
                FROM wpprod.wp_" . (string)$wpTableNumber . "_postmeta
                where meta_key like '%_menu_item_url%'
                and meta_value like '%facebook%') as facebook on facebook.meta_key = facebook.meta_key
            left join 
                (select meta_key, meta_value
                FROM wpprod.wp_" . (string)$wpTableNumber . "_postmeta
                where meta_key like '%_menu_item_url%'
                and meta_value like '%insta%') as insta on insta.meta_key = insta.meta_key    
            left join 
                (select meta_key, meta_value
                FROM wpprod.wp_" . (string)$wpTableNumber . "_postmeta
                where meta_key like '%_menu_item_url%'
                and meta_value like '%twitter%') as twitter on twitter.meta_key = twitter.meta_key
            inner join
                (select blog.option_value as blog, url.option_value as url   
                from
                (select option_value, option_name 
                    from wpprod.wp_" . (string)$wpTableNumber . "_options 
                    where option_name = 'blogname') as blog 
                left join 
                (select option_value, option_name 
                    from wpprod.wp_" . (string)$wpTableNumber . "_options 
                    where option_name = 'siteurl') as url on url.option_value = url.option_value) as clubNameAndUrl on clubNameAndUrl.blog = clubNameAndUrl.blog
            group by url";

    $queryResult = $connection->query($sqlQuery);

    if ($queryResult->num_rows > 0){
        $tableCounter++;
        while($row = $queryResult->fetch_assoc()) {
            echo "<tr>
                    <td>" . $wpTableNumber. "</td>
                    <td>" . str_replace(".site.kiwanis.org", "", (str_replace("http://","",$row["url"]))) . "</td>
                    <td>" . $row["clubName"]. "</td>
                    <td>" . $row["url"]. "</td>
                    <td>" . $row["Facebook"]. "</td>
                    <td>" . $row["Twitter"]. "</td>
                    <td>" . $row["Instagram"]. "</td>
                  </tr>";
        }
    }

    $wpTableNumber++;
}
?>

</table>


<?php

$queryResult->close();

mysqli_close($connection);

?>
