<?php

require_once ("config/dbConfig.php");

class Connection{

//    public function __construct(){
//
//    }

    protected $db_name = DB_NAME;
    protected $db_user = DB_USER;
    protected $db_pass = DB_PASSWORD;
    protected $db_host = DB_HOST;
    protected $link;

    public function dBConnect(){

        $this->link = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name);

        if (!$this->link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

        return $this->link;
    }


}
